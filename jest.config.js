module.exports = {
  moduleDirectories: ['node_modules'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  setupFilesAfterEnv: ['<rootDir>/src/tests/setupTests.js'],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
  },
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/src/tests/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/src/tests/__mocks__/styleMock.js',
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  collectCoverageFrom: [
    'src/**/*.{js,ts,tsx}',
    '!src/**/index.{js,ts}',
    '!src/**/*.stories.{js,ts}',
    '!src/**/interfaces/*',
    '!src/pages/*',
  ],
  coverageThreshold: {
    global: {
      branches: 48,
      functions: 60,
      lines: 65,
      statements: 64,
    },
  },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputName: 'coverage/junit-jest.xml',
      },
    ],
  ],
}
