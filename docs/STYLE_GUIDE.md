# 1 Introduction

This document serves as the complete definition of Junyi’s coding standards for source code in the JavaScript programming language. A JavaScript source file is described as being in Junyi Style if and only if it adheres to the rules herein.

Like other programming style guides, the issues covered span not only aesthetic issues of formatting, but other types of conventions or coding standards as well. However, this document focuses primarily on the hard-and-fast rules that we follow universally, and avoids giving advice that isn't clearly enforceable (whether by human or tool).

## 1.1 Terminology notes

In this document, unless otherwise clarified:

1.  The term comment always refers to implementation comments. We do not use the phrase documentation comments, instead using the common term “JSDoc” for both human-readable text and machine-readable annotations within `/** … */`.

2.  This Style Guide uses [RFC 2119](https://tools.ietf.org/html/rfc2119) terminology when using the phrases must, must not, should, should not, and may. The terms prefer and avoid correspond to should and should not, respectively. Imperative and declarative statements are prescriptive and correspond to must.

Other terminology notes will appear occasionally throughout the document.

## 1.2 Guide notes

Example code in this document is non-normative. That is, while the examples are in Junyi Style, they may not illustrate the only stylish way to represent the code. Optional formatting choices made in examples must not be enforced as rules.

# 2 Source file basics

## 2.1 File name

React file names must be UpperCamelCase. Others must be lowerCamelCase. Filenames’ extension must be `.js`.

# 3 Source file structure

All new source files must be an ECMAScript (ES) module (uses `import` and `export` statements).

Files consist of the following, in order:

1.  License or copyright information, if present
2.  `@fileoverview` JSDoc, if present
3.  ES `import` statements, if an ES module
4.  The file’s implementation
5.  ES `export` statements, if an ES module

**Exactly one blank line** separates each section that is present.

## 3.1 License or copyright information, if present

If license or copyright information belongs in a file, it belongs here.

## 3.2 @fileoverview JSDoc, if present

See [google JavaScript style guide 7.5 Top/file-level comments for formatting rules](https://google.github.io/styleguide/jsguide.html#jsdoc-top-file-level-comments).

## 3.3 ES `import` statements, if an ES module

### 3.3.1 Order of import statements

Import statements consist of the following, in order:

#### 3.3.1.1 React presentational components

1.  Top imports
    - Import `React`
    - Import packages from `node_modules` (prefer alphabet ordering)
    - Import Material UI `useMediaQuery` by default import
    - import Material UI `styles` by name import
    - import Material UI `components` by name import
    - import Material UI `icons` by name import
1.  Import `types`
1.  Import `utils`
1.  Import `assets`
1.  Import `components`

#### 3.3.1.2 React container components

1.  Top imports
    - Import `React`
    - Import `connect` and `ConnectedProps`
    - Import `bindActionCreators` and `Dispatch`
    - Import packages from `node_modules` (prefer alphabet ordering)
    - Import Material UI `useMediaQuery` by default import
    - import Material UI `styles` by name import
    - import Material UI `components` by name import
    - import Material UI `icons` by name import
1.  Import `types`
1.  Import `utils`
1.  Import `assets`
1.  Import `selectors`
1.  Import `actions`
1.  Import `components`

#### 3.3.1.3 Unit test files

1.  Import packages form `node_modules`
2.  Import `utils`
3.  Import target components

#### 3.3.1.4 Other files

1.  Import packages from `node_modules`
2.  Import other files

### 3.3.2 Import paths

#### 3.3.2.1 Import from the same package

Import paths must be relative paths.

😀😀😀

```js
import './sideEffects'

import * as parent from '../parents'

import App from './App'
```

#### 3.3.2.2 Import from the outside packages

Import paths must be absolute with alias `@`

😀😀😀

```js
import { StudentLayout } from '@/packages/layout'
import { requireDeveloper } from '@/packages/permission'
```

😡😡😡

```js
import { StudentLayout } from '../../packages/layout'
import { requireDeveloper } from '../../packages/permission'
```

#### 3.3.2.3 File extensions in import paths

The .js, .ts and .tsx file extensions are not optional in import paths and must always be excluded.

😀😀😀

```js
import '../directory/file'
```

😡😡😡

```js
import '../directory/file.js'
```

#### 3.3.2.4 Importing the same file multiple times

Do not import the same file multiple times. This can make it hard to determine the aggregate imports of a file.

😡😡😡

```js
// Imports have the same path, but since it doesn't align it can be hard to see.
import { short } from './long/path/to/a/file'
import { aLongNameThatBreaksAlignment } from './long/path/to/a/file'
```

## 3.4 The file’s implementation

### 3.4.1 React presentational components

1.  Define the file-scoped configs
2.  Define the components
    - Styles with Material UI `makeStyles` if needed
    - Styled components with Material UI `styled` if needed
    - Components only used in this file if needed
    - type `Props`
    - Components with name export if needed
    - Component with default export the same name of its file's name

### 3.4.2 React container components

1.  Define the file-scoped configs
2.  Define the components
    - Styles with Material UI `makeStyles` if needed
    - Styled components with Material UI `styled` if needed
    - Components only used in this file if needed
    - type `Props` composed by `PropsFromRedux` and `PropsFromSelf`
    - Components with name export if needed
    - Component with default export the same name of its file's name

### 3.4.3 Unit test files

1.  Setup test (ex: `setupServer`)
2.  The `test`s

## 3.5 ES `export` statements, if an ES module

### 3.5.1 React presentational and container components

Default export the react component with the same name of the file.

### 3.5.2 Other files

Use named exports in all code. You can `apply` the export keyword to a declaration, or use the `export { name }` syntax.

Do not use default exports. Importing modules must give a name to these values, which can lead to inconsistencies in naming across modules.

😡😡😡

```js
// Do not use default exports:
export default class Foo { ... } // BAD!
```

😀😀😀

```js
// Use named exports:
export class Foo { ... }
```

😀😀😀

```js
// Alternate style named exports:
class Foo { ... }

export { Foo };
```

# 4 Formatting

Use prettier to format all JavaScript source files.

## 4.1 Comments

### 4.1.1 Block comment style

Block comments are indented at the same level as the surrounding code. They may be in `/* … */` or `//`-style. For multi-line `/* … */` comments, subsequent lines must start with `*` aligned with the `*` on the previous line, to make comments obvious with no extra context.

😀😀😀

```js
/*
 * This is
 * okay.
 */

// And so
// is this.

/* This is fine, too. */
```

Comments are not enclosed in boxes drawn with asterisks or other characters.

Do not use JSDoc (`/** … */`) for implementation comments.

### 4.1.2 Parameter Name Comments

“Parameter name” comments should be used whenever the value and method name do not sufficiently convey the meaning, and refactoring the method to be clearer is infeasible. Their preferred format is before the value with "=":

😀😀😀

```js
someFunction(obviousParam, /* shouldRender= */ true, /* name= */ 'hello')
```

# 5 Language features

Follow [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html#language-features)

# 6 Naming

Follow [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html#naming)

## 6.1 React handler functions

### Names

#### 6.1.1 Implementation

Handler functions must be named as `handle + Something + Action` when implemented.

😀😀😀

```js
function handleButtonClick() {}
```

😡😡😡

```js
function handleClickButton() {}

function handleClick() {}

function onClickButton() {}

function onClick() {}
```

#### 6.1.2 Pass down as props

Handler functions must be rename as `on + Something + Action` when passed to components as props.

😀😀😀

```js
<Component onButtonClick={handleButtonClick} />
```

😡😡😡

```js
<Component handleClick={handleButtonClick} />

<Component handleButtonClick={handleButtonClick} />
```

# 7 JSDoc

Follow [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html#jsdoc)

# 8 Material UI

## 8.1 `makeStyles` API

`makeStyles(styles, [options]) => hook`

Link a style sheet with a function component using the hook pattern.

See [Material-UI Styles API documentation](https://material-ui.com/styles/api/#makestyles-styles-options-hook)

### 8.1.1 `styles`

#### 8.1.1.1 `styles` type

First argument of `makeStyles` must be a function if you need to access the theme. Or it must be a styles object.

😀😀😀

```js
const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
  },
}))
```

😀😀😀

```js
const useStyles = makeStyles({
  root: {
    color: 'red',
  },
})
```

😡😡😡

```js
const useStyles = makeStyles(() => ({
  root: {
    color: 'red',
  },
}))
```

#### 8.1.1.2 Props extracting level

Props must be extracted at style rule level.

😀😀😀

```js
const useStyles = makeStyles({
  // style rule
  root: (props) => ({
    color: props.color,
  }),
})
```

😡😡😡

```js
const useStyles = makeStyles({
  root: {
    // CSS property
    color: (props) => props.color,
  },
})
```

### 8.1.2 `options`

#### 8.1.2.1 Style sheet name

`options.name` must not be set. It would be set if you are debugging.

😀😀😀

```js
const useStyles = makeStyles(styles)
```

😡😡😡

```js
const useStyles = makeStyles(styles, { name: 'CustomName' })
```

### 8.1.3 `hook` and `useStyles`

#### 8.1.3.1 Hook naming

Hook return from `makeStyles` must be named as `useStyles` or `useComponentNameStyles`.

😀😀😀

```js
const useComponentNameStyles = makeStyles(styles)

const ComponentName = () => {
  const componentNameClasses = useComponentNameStyles()
}
```

😀😀😀

```js
const useStyles = makeStyles(styles)

const ComponentName = () => {
  const classes = useStyles()
}
```

## 8.2 `styled` API

`styled(Component)(styles, [options]) => Component`

Link a style sheet with a function component using the styled components pattern.

See [Material-UI Styles API documentation](https://material-ui.com/styles/api/#styled-component-styles-options-component)

### 8.2.1 `styles`

#### 8.2.1.1 `styles` type

`styles` must be a function if you need to access the theme or props. Or it must be a styles object.

😀😀😀

```js
const StyledDiv = styled('div')(({ theme }) => ({
  color: theme.palette.primary.main,
}))
```

😀😀😀

```js
const StyledDiv = styled('div')({
  backgroundColor: 'red',
})
```

😡😡😡

```js
const StyledDiv = styled('div')(() => ({
  backgroundColor: 'red',
}))
```

#### 8.2.1.2 Props extracting level

Props must be extracted at styles function level, not at CSS property level.

😀😀😀

```js
const StyledDiv = styled(({ backgroundColor: _backgroundColor, ...other }) => (
  <div {...other} />
))(({ backgroundColor }) => ({
  backgroundColor,
}))
```

😡😡😡

```js
const StyledDiv = styled(({ backgroundColor: _backgroundColor, ...other }) => (
  <div {...other} />
))({
  // CSS property
  backgroundColor: ({ backgroundColor }) => backgroundColor,
})
```

#### 8.2.1.3 Ignore pattern of Props extracting

Add `_` in front of the extracted prop name to avoid eslint error.

😀😀😀

```js
const StyledDiv = styled(({ foo: _foo, ...other }) => (
  <div {...other} />
))(({ foo }) => ({}))
```

😡😡😡

```js
const StyledDiv = styled(({ foo: _a, ...other }) => (
  <div {...other} />
))(({ foo }) => ({}))
```

### 8.2.2 `options`

#### 8.2.2.1 Style sheet name

`options.name` must not be set. It would be set if you are debugging.

😀😀😀

```js
const StyledDiv = styled('div')(styles)
```

😡😡😡

```js
const StyledDiv = styled('div')(styles, { name: 'CustomName' })
```

## 8.3 `<Hidden>...</Hidden>`

You must not use `<Hidden>...</Hidden>` because it's hard to test. Use `useMediaQuery` instead.

## 8.4 useMediaQuery

Boolean value return from `useMediaQuery` must be named as follows:

1.  isDesktop
2.  isTablet
3.  isMobile

😀😀😀

```js
const isDesktop = useMediaQuery(theme.breakpoints.up('lg'))
```

## 8.5 theme.spacing

😀😀😀

```js
margin: theme.spacing(1, 0)
```

Disallowed:

😡😡😡

```js
margin: [[theme.spacing(1), 0]]
```

# 9 TypeScript

## 9.1 Interface Naming

Name of interface must begin with a capital `I`.

😀😀😀

```js
interface ICat {}
```

😡😡😡

```js
interface iCat {}

interface Cat {}

interface cat {}
```

## 9.2 Import statement

Import statements for types must add a `type` after `import`.

😀😀😀

```js
import type { ICat } from '/some/where'
```

😡😡😡

```js
import { ICat } from '/some/where'
```
