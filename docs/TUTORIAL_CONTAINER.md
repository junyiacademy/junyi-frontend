# Introduction

Container is a place to connect react world and redux world.

`connect` avoid re-render problems like `React.memo`. So we use `connect` instead of hooks.

Check [Blogged Answers: A (Mostly) Complete Guide to React Rendering Behavior](https://blog.isquaredsoftware.com/2020/05/blogged-answers-a-mostly-complete-guide-to-react-rendering-behavior/?utm_campaign=React%2BNewsletter&utm_medium=email&utm_source=React_Newsletter_213) for more information.

# Example

## A simple container

```jsx
// src/packages/adoption/containers/CatList.tsx

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import type { IRootState } from '@/store'

// utils
import { namespace } from '../redux/slice'

// assets

// selectors

// actions
import { fetchCatsAsync } from '../redux/slice'

// components
import CatListComponent from '../components/CatList' // rename CatList to avoid name conflict

// self-defined-components

const CatList = ({ cats, isFetchingCats }: Props) => (
  <CatListComponent cats={cats} isFetchingCats={isFetchingCats} />
)

const mapStateToProps = (state: IRootState) => ({
  cats: state[namespace].cats,
  isFetchingCats: state[namespace].isFetchingCats,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchCatsAsync }, dispatch)

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(CatList)
```

# Unit Test

Use the custom render that includes all providers we need.

## Test with initial state

```ts
// src/packages/adoption/containers/__tests__/CatList.js

import React from 'react'

// utils
import { render } from '@/tests'

// components
import CatList from '../CatList'

test('should be rendered correctly', () => {
  // Arrange
  const options = {
    initialState: {},
  }

  render(<CatList />, options)

  // Act

  // Assert
})
```

## Test with provided state

```ts
// src/packages/adoption/containers/__tests__/CatList.js

import React from 'react'

// utils
import { render } from '@/tests'

// components
import CatList from '../CatList'

test("should show loading when it's fetching the cats", () => {
  // Arrange
  const options = {
    initialState: {
      adoption: {
        cats: [],
        isFetchingCats: true,
      },
    },
  }

  render(<CatList />, options)

  // Act

  // Assert
})
```

## Test with mock data and test-id

```ts
// src/packages/adoption/containers/__tests__/CatList.js

import React from 'react'

// utils
import { render, screen } from '@/tests'

// mocks
import { generateRandomCat } from '../../__mocks__/cat'

// components
import CatList from '../CatList'

// self-defined-config
const TEST_ID_LOADING = 'adoption-cat-loading'

// self-defined-mocks

// tests
test("should show loading when it's fetching the cats", () => {
  // Arrange
  const options = {
    initialState: {
      adoption: {
        cats: [],
        isFetchingCats: true,
      },
    },
  }

  render(<CatList />, options)

  // Act

  // Assert
  expect(screen.queryByTestId(TEST_ID_LOADING)).toBeInTheDocument()
})

test('should show cat cards when state.adoption.cats exists', () => {
  // Arrange
  const mockCats = Array.from({ length: 10 }).map(generateRandomCat)
  const options = {
    initialState: {
      adoption: {
        cats: mockCats,
        isFetchingCats: false,
      },
    },
  }

  render(<CatList />, options)

  // Act

  // Assert
  expect(screen.queryByTestId(TEST_ID_LOADING)).not.toBeInTheDocument()
  mockCats.foreach(({ name }) =>
    expect(screen.queryByText(name)).toBeInTheDocument()
  )
})
```

## Test with mock api

Check [https://testing-library.com/docs/react-testing-library/example-intro/#mock](https://testing-library.com/docs/react-testing-library/example-intro/#mock) for more information

# To be discussed

1. If the container want to connect to multiple state slice, what should the namespace be renamed as?
