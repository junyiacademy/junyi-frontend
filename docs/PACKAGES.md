# Introduction

Package is a basic unit for grouping the related functionality.

**Note:** Packages should be placed under `/src/packages/`.

# Import and export

## Import from other packages

Use alias to import from other packages. ex: `import CatList from '@/packages/adoption'`

Only import from the `@/packages/<package_name>/index.ts` because it's easier to manage the dependency.

In other world, export everything allowed to be imported by other packages in your package root `index.ts`.

## Import from current packages

Use relative path to import from the current package. ex: `import CatList from '../components/CatList'`

Import from the exact file directly, not from the `<path>/index.ts`.

# Create new package

`$ npm run init:package`

The generated folder will looks like:

```
.
├── __mocks__/
├── assets/
├── components/
│   └── __tests__/
├── containers/
│   └── __tests__/
├── interfaces/
├── redux/
│   ├── __tests__/
│   ├── epics.ts (Epics created by redux-observable and rxjs)
│   ├── selectors.ts (Selectors created by @reduxjs/toolkit)
│   ├── slice.ts (Slice created by @reduxjs/toolkit)
│   └── types.ts (Types for slice and type guard)
├── utils/
│   └── __tests__/
└── index.ts (Export everything allowed to be used by other packages)
```

# To be discussed

1. Is there better way to manage the grouped functionality?
1. Is there better way to manage the dependency?
