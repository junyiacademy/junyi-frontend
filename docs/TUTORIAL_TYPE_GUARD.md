# Introduction

Type Guards allow you to narrow down the type of an object within a conditional block.

We use type guards to ensure the shape of api response is correct.

Check [Type Guard](https://basarat.gitbook.io/typescript/type-system/typeguard) for more information.

# Example

```ts
export function assertIsTyped<T>(
  arg: any,
  check: (val: any) => val is T
): asserts arg is T {
  if (!check(arg)) {
    throw new Error(`Violators found: ${JSON.stringify(arg)}`)
  }
}

export interface ISuccessResponse {
  data: Record<string, any>
}

function isSuccessResponse(arg: any): arg is ISuccessResponse {
  return typeof arg.data === 'object' && typeof arg.error === 'null'
}

export function assertIsSuccessResponse(arg: any) {
  assertIsTyped(arg, isSuccessResponse)
  return arg
}
```

## A simple use case

```ts
from(get('/api/cats', {})).pipe(
  map((response) => assertIsSuccessResponse(response)), // response is unknown
  map((response) => response.data) // response is ISuccessResponse
)
```

You can go `src/packages/courseMenu/redux/epics.ts` and hover the response to check its type.
