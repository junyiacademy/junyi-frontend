/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const { program } = require('commander')
const { prompt } = require('inquirer')
const chalk = require('chalk')
const fs = require('fs')

const junyiCopyRightAndLicense = `/**
  * Copyright (c) 2021 Junyi Academy.
  *
  * This source code is licensed under the MIT license found in the
  * LICENSE file in the root directory of this source tree.
  */
 `
const packagePathRoot = 'src/packages'
const getPackagePath = (packageName) => `${packagePathRoot}/${packageName}`
const config = [
  { subPath: '/', files: ['index.ts'] },
  { subPath: '/__mocks__', files: [] },
  { subPath: '/assets', files: [] },
  { subPath: '/components', files: [] },
  { subPath: '/components/__tests__', files: [] },
  { subPath: '/containers', files: [] },
  { subPath: '/containers/__tests__', files: [] },
  { subPath: '/interfaces', files: [] },
  {
    subPath: '/redux',
    files: ['epics.ts', 'selectors.ts', 'slice.ts', 'types.ts'],
  },
  { subPath: '/redux/__tests__', files: [] },
  { subPath: '/utils', files: [] },
  { subPath: '/utils/__tests__', files: [] },
]

program.version('1.0.0').description('Junyi Frontend CLI program')

const createFolder = (path, subPath) => {
  fs.mkdirSync(`${path}${subPath}`)
  console.log(`${path}${chalk.blueBright(subPath)}`)
}

const createFile = (path, subPath, filename) => {
  fs.writeFileSync(`${path}${subPath}/${filename}`, junyiCopyRightAndLicense)
  if (filename !== 'index.ts') {
    console.log(
      `${path}${chalk.blueBright(subPath + '/')}${chalk.greenBright(filename)}`
    )
  }
}

program
  .command('new')
  .alias('n')
  .description('add a new package')
  .action(() =>
    prompt({
      type: 'input',
      name: 'packageName',
      message: 'enter your package name',
    })
      .then((answers) => {
        if (answers.packageName === '') {
          throw new Error('not a valid package name')
        }

        return answers.packageName
      })
      .then(getPackagePath)
      .then((path) => {
        if (fs.existsSync(path)) {
          throw new Error(`${path} exists`)
        }

        console.log('start creating your package ...')

        config.forEach(({ subPath, files }) => {
          createFolder(path, subPath)
          files.forEach((filename) => createFile(path, subPath, filename))
        })
      })
      .catch((err) => console.error(chalk.redBright(err)))
  )

program.parse(process.argv)
