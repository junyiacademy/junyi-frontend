/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { GetServerSideProps } from 'next'
import { StudentLayout } from '@/packages/layout'
import { requireDeveloper } from '@/packages/permission'
import { fetchPermission, getAuthToken } from '@/utils'
import Typography from '@material-ui/core/Typography'

function Index() {
  return (
    <StudentLayout>
      <Typography variant='h1'>Hi</Typography>
    </StudentLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const cookies = req.headers.cookie || ''

  const serverSideAuthToken = getAuthToken(cookies)

  const [permission] = await Promise.all([fetchPermission(cookies)])

  return {
    props: {
      serverSideAuthToken,
      initialReduxState: {
        permission,
      },
    }, // will be passed to the page component as props
  }
}

export default requireDeveloper(Index)
