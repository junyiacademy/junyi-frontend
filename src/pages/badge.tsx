/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { StudentLayout } from '@/packages/layout'
import { BadgeMenu } from '@/packages/badgeMenu/containers'

function Badge() {
  return (
    <StudentLayout>
      <BadgeMenu />
    </StudentLayout>
  )
}

export default Badge
