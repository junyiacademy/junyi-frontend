/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import type { NextApiRequest, NextApiResponse } from 'next'
import cookie from 'cookie'

type Data = {
  roles?: string[]
  message?: string
}

export default async function permission(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  // delay 1.5s for demo
  await new Promise((resolve) => setTimeout(() => resolve({}), 1500))

  if (req.method === 'GET') {
    const cookies = cookie.parse(req.headers.cookie || '')
    const authToken = cookies._junyi_session

    if (authToken) {
      res.json({ roles: ['developer'] })
    } else {
      res.status(400).json({ roles: [] })
    }
  } else {
    res.status(405).json({ message: 'We only support GET' })
  }
}
