/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import type { NextApiRequest, NextApiResponse } from 'next'

const mockData = [
  {
    title: '數學',
    href: '',
    groups: [
      {
        title: '國小',
        items: [
          {
            href: '/course-compare/math-grade-1-a',
            title: '一年級',
          },
          {
            href: '/course-compare/math-grade-2-a',
            title: '二年級',
          },
          {
            href: '/course-compare/math-grade-3-a',
            title: '三年級',
          },
          {
            href: '/course-compare/math-grade-4-a',
            title: '四年級',
          },
          {
            href: '/course-compare/math-grade-5-a',
            title: '五年級',
          },
          {
            href: '/course-compare/math-grade-6-a',
            title: '六年級',
          },
        ],
      },
      {
        title: '國中',
        items: [
          {
            href: '/course-compare/math-grade-7-a',
            title: '七年級',
          },
          {
            href: '/course-compare/math-grade-8-a',
            title: '八年級',
          },
          {
            href: '/course-compare/math-grade-9-a',
            title: '九年級',
          },
        ],
      },
      {
        title: '高中',
        items: [
          {
            href: '/course-compare/math-grade-10',
            title: '十年級',
          },
          {
            href: '/course-compare/math-grade-11',
            title: '十一年級',
          },
          {
            href: '/course-compare/math-grade-12',
            title: '十二年級',
          },
        ],
      },
      {
        title: '大學先修',
        items: [
          {
            href: '/course-compare/calculus_1',
            title: '微積分',
          },
          {
            href: '/course-compare/junyi-linear-algebra_1',
            title: '線性代數',
          },
        ],
      },
    ],
  },
  {
    title: '素養',
    href: '/junyi-competency',
    groups: [
      {
        title: '',
        items: [
          {
            href: '/junyi-competency/logic',
            title: '思考力訓練',
          },
          {
            href: '/junyi-competency/financial-management',
            title: '理財能力',
          },
          {
            href: '/junyi-competency/v972-new-topic',
            title: '科學議題',
          },
          {
            href: '/junyi-competency/v1093-new-topic-1',
            title: '科學媒體素養',
          },
        ],
      },
    ],
  },
  {
    title: '自然',
    href: '/junyi-science',
    groups: [
      {
        title: '國中',
        items: [
          {
            href: '/junyi-science/junyi-middle-school-biology',
            title: '國中生物',
          },
          {
            href: '/junyi-science/middle-school-physics-chemistry',
            title: '國中理化',
          },
          {
            href: '/junyi-science/junyi-middle-earth-science',
            title: '國中地科',
          },
          {
            href: '/junyi-science/lis-science',
            title: 'LIS 情境科學教材',
          },
        ],
      },
      {
        title: '高中',
        items: [
          {
            href: '/junyi-science/junyi-biology',
            title: '高中生物',
          },
          {
            href: '/junyi-science/junyi-physics',
            title: '高中物理',
          },
          {
            href: '/junyi-science/junyi-chemistry',
            title: '高中化學',
          },
          {
            href: '/junyi-science/dr-go-high-school-earth-science',
            title: '高中地科',
          },
        ],
      },
    ],
  },
  {
    title: '電腦科學',
    href: '/computing',
    groups: [
      {
        title: '主題式',
        items: [
          {
            href: '/computing/computer-science',
            title: '科技科普',
          },
          {
            href: '/computing/programming',
            title: '程式設計',
          },
          {
            href: '/computing/electrical-engineering',
            title: '機電整合',
          },
          {
            href: '/computing/cs-isl',
            title: '數位素養',
          },
          {
            href: '/computing/cross-discipline',
            title: '跨域學習',
          },
          {
            href: '/computing/sw-app',
            title: '軟體應用',
          },
        ],
      },
    ],
  },
  {
    title: '英文',
    href: '/junyi-english',
    groups: [
      {
        title: '主題式',
        items: [
          {
            href: '/junyi-english/e1a',
            title: '認識英文字母',
          },
          {
            href: '/junyi-english/eephn',
            title: '自然發音',
          },
          {
            href: '/junyi-english/eer',
            title: '英文閱讀素養',
          },
          {
            href: '/junyi-english/happyenglishlearning_new1',
            title: '用中學英語',
          },
        ],
      },
      {
        title: '年段',
        items: [
          {
            href: '/junyi-english/eng-elementary',
            title: '國小',
          },
          {
            href: '/junyi-english/eng-junior',
            title: '國中',
          },
          {
            href: '/junyi-english/eng-senior',
            title: '高中',
          },
        ],
      },
    ],
  },
  {
    title: '社會',
    href: '/junyi-society',
    groups: [
      {
        title: '',
        items: [
          {
            href: '/junyi-society/middle-school-civics',
            title: '國中公民',
          },
          {
            href: '/junyi-society/junyi-geography',
            title: '高中地理',
          },
          {
            href: '/junyi-society/history',
            title: '歷史',
          },
        ],
      },
    ],
  },
  {
    title: '興趣探索',
    href: '/v1051-new-topic',
    groups: [
      {
        title: '',
        items: [
          {
            href: '/v1051-new-topic/shiny-diabolo',
            title: '扯鈴',
          },
          {
            href: '/v1051-new-topic/dancing',
            title: '舞蹈',
          },
          {
            href: '/v1051-new-topic/junyi-music',
            title: '音樂',
          },
          {
            href: '/v1051-new-topic/hour-of-cs',
            title: '一小時玩程式',
          },
        ],
      },
    ],
  },
  {
    title: '均一好朋友',
    href: '/many-great-teachers',
    groups: [
      {
        title: '',
        items: [
          {
            href: '/many-great-teachers/cooc2019',
            title: '酷課雲專區',
          },
          {
            href: '/many-great-teachers/nani-project',
            title: '南一專區',
          },
          {
            href: '/many-great-teachers/hanlin-project',
            title: '翰林專區',
          },
          {
            href: '/many-great-teachers/cool-english',
            title: '酷英專區',
          },
          {
            href: '/many-great-teachers/tw_bar',
            title: '臺灣吧專區',
          },
          {
            href: '/many-great-teachers/mby',
            title: '博幼專區',
          },
          {
            href: '/many-great-teachers/vea01',
            title: '認識越南語字母',
          },
          {
            href: '/many-great-teachers/doctor-huang',
            title: '黃瑽寧醫師健康講堂',
          },
        ],
      },
    ],
  },
  {
    title: '評量專區',
    href: '/junyi-test',
    groups: [
      {
        title: '',
        items: [
          {
            href: '/junyi-test/junyi-remedial',
            title: '學習扶助複習測驗',
          },
          {
            href: '/junyi-test/hka-prep',
            title: '國三會考複習',
          },
          {
            href: '/junyi-test/sdcxc-prep',
            title: '高三學測複習',
          },
          {
            href: '/junyi-test/sdczk-prep',
            title: '高三指考複習',
          },
          {
            href: '/junyi-test/sdctc',
            title: '高職統測',
          },
        ],
      },
    ],
  },
]

export default async function courseMenu(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // delay 1.5s for demo
  await new Promise((resolve) => setTimeout(() => resolve({}), 1500))

  if (req.method === 'GET') {
    res.json({ subjects: mockData })
  } else {
    res.status(405).json({ message: 'We only support GET' })
  }
}
