/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import type { NextApiRequest, NextApiResponse } from 'next'

const mockData = [
  {
    badges: [
      {
        description: '三疊紀',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: true,
        safeExtendedDescription:
          '任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟',
      },
      {
        description: '三疊紀-2',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: false,
        safeExtendedDescription: '任何 3 個技能達到精熟',
      },
      {
        description: '三疊紀-3',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: true,
        safeExtendedDescription: '任何 3 個技能達到精熟',
      },
      {
        description: '三疊紀-4',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: false,
        safeExtendedDescription:
          '你在 2020/5/5 ～ 2020/5/28 挑戰期間，觀看了「水平思考 VS 垂直思考」、「若 P 則 Q」、「稻草人謬誤」、「合理不等於正確」、「非黑即白的思考模式」這 5 支影片，並完成了對應影片的習題升級，你主動積極的學習態度超厲害的，往後也要將思考力運用於生活中喔！',
      },
    ],
    category: 0,
    categoryIcon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
    categoryDescription: '在廣闊宇宙裡隨處可見，代表孩子登上了浩瀚的學習宇宙。',
    typeLabel: '隕石',
    userBadges: [
      {
        description: '三疊紀',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: true,
        lastEarnedDate: '2020-11-12T08:48:13Z',
        safeExtendedDescription:
          '任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟任何 3 個技能達到精熟',
      },
      {
        description: '三疊紀-3',
        icon: 'https://www.junyiacademy.org/images/badges/meteorite-v2.png',
        isOwned: true,
        lastEarnedDate: '2020-12-12T08:48:13Z',
        safeExtendedDescription: '任何 3 個技能達到精熟',
      },
    ],
  },
  {
    badges: [
      {
        description: '侏羅紀',
        icon: 'https://www.junyiacademy.org/images/badges/moon-v2.png',
        isOwned: false,
        safeExtendedDescription: '任何 15 個技能達到精熟',
      },
    ],
    category: 1,
    categoryIcon: 'https://www.junyiacademy.org/images/badges/moon-v2.png',
    categoryDescription:
      '廣闊宇宙裡閃耀的一顆星，象徵孩子不斷努力，持續對學習的付出。',
    typeLabel: '月亮',
    userBadges: [],
  },
  {
    badges: [
      {
        description: '白堊紀',
        icon: 'https://www.junyiacademy.org/images/badges/earth-v2.png',
        isOwned: true,
        safeExtendedDescription: '任何 50 個技能達到精熟',
      },
    ],
    category: 2,
    categoryIcon: 'https://www.junyiacademy.org/images/badges/earth-v2.png',
    categoryDescription:
      '美麗、富有資源的象徵，代表孩子在學習上成功找到資源、方法。',
    typeLabel: '地球',
    userBadges: [
      {
        description: '第四紀',
        icon: 'https://www.junyiacademy.org/images/badges/earth-v2.png',
        isOwned: true,
        lastEarnedDate: '2021-01-22T08:48:13Z',
        safeExtendedDescription: '任何 150 個技能達到精熟',
      },
    ],
  },
  {
    badges: [
      {
        description: '古近紀',
        icon: 'https://www.junyiacademy.org/images/badges/sun-v2.png',
        isOwned: true,
        safeExtendedDescription: '任何 80 個技能達到精熟',
      },
    ],
    category: 3,
    categoryIcon: 'https://www.junyiacademy.org/images/badges/sun-v2.png',
    categoryDescription:
      '傳說、史詩級的象徵，代表孩子經歷烈焱的考驗後才能獲得。',
    typeLabel: '太陽',
    userBadges: [
      {
        description: '第四紀',
        icon: 'https://www.junyiacademy.org/images/badges/sun-v2.png',
        isOwned: true,
        lastEarnedDate: '2019-11-29T08:48:13Z',
        safeExtendedDescription: '任何 150 個技能達到精熟',
      },
    ],
  },
  {
    badges: [
      {
        description: '新近紀',
        icon: 'https://www.junyiacademy.org/images/badges/eclipse-v2.png',
        isOwned: true,
        safeExtendedDescription: '任何 100 個技能達到精熟',
      },
    ],
    category: 4,
    categoryIcon: 'https://www.junyiacademy.org/images/badges/eclipse-v2.png',
    categoryDescription:
      '傳說、神秘的象徵，任何嘗試接近的一切都將被捲入未知黑洞。',
    typeLabel: '黑洞',
    userBadges: [
      {
        description: '第四紀',
        icon: 'https://www.junyiacademy.org/images/badges/eclipse-v2.png',
        isOwned: true,
        lastEarnedDate: '2020-07-29T08:48:13Z',
        safeExtendedDescription: '任何 150 個技能達到精熟',
      },
    ],
  },
  {
    badges: [
      {
        description: '第四紀',
        icon:
          'https://www.junyiacademy.org/images/badges/master-challenge-blue-v2.png',
        isOwned: true,
        safeExtendedDescription: '任何 150 個技能達到精熟',
      },
    ],
    category: 5,
    categoryIcon:
      'https://www.junyiacademy.org/images/badges/master-challenge-blue-v2.png',
    categoryDescription:
      '面對未知也能繼續勇往直前的勇氣，象徵孩子在宇宙裡無畏的勇氣與不放棄的精神。',
    typeLabel: '挑戰',
    userBadges: [
      {
        description: '第四紀',
        icon:
          'https://www.junyiacademy.org/images/badges/master-challenge-blue-v2.png',
        isOwned: true,
        lastEarnedDate: '2020-10-29T08:48:13Z',
        safeExtendedDescription: '任何 150 個技能達到精熟',
      },
    ],
  },
]

export default async function badgeMenu(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await new Promise((resolve) => setTimeout(() => resolve({}), 1500))

  if (req.method === 'GET') {
    res.json({ badgeCollections: mockData })
  } else {
    res.status(405).json({ message: 'We only support GET' })
  }
}
