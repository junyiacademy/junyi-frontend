/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineReducers } from 'redux'

// reducers
import { reducer as permission } from '@/packages/permission/redux'
import { reducer as courseMenu } from '@/packages/courseMenu/redux'
import { reducer as badgeMenu } from '@/packages/badgeMenu/redux'

const rootReducer = combineReducers({ permission, courseMenu, badgeMenu })

export type IRootState = ReturnType<typeof rootReducer>

export default rootReducer
