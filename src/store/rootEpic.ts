/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics, Epic, StateObservable } from 'redux-observable'
import { catchError } from 'rxjs/operators'

// epics
import { epic as permissionEpic } from '@/packages/permission/redux'
import { epic as courseMenuEpic } from '@/packages/courseMenu/redux'
import { epic as badgeEpic } from '@/packages/badgeMenu/redux'
import { IRootState } from './rootReducer'

const epics = [permissionEpic, courseMenuEpic, badgeEpic]

const rootEpic: Epic = (
  action$,
  store$: StateObservable<IRootState>,
  dependencies
) =>
  combineEpics(...epics)(action$, store$, dependencies).pipe(
    catchError((error, source) => {
      console.error(error)
      return source
    })
  )

export default rootEpic
