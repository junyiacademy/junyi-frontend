/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import axios from 'axios'

export const get = (endpoint: string, query: Record<string, any>) =>
  axios.get(endpoint, { params: query })
export const post = (endpoint: string, query: Record<string, any>) =>
  axios.post(endpoint, query)
