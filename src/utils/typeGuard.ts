/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function assertIsTyped<T>(
  arg: any,
  check: (val: any) => val is T
): asserts arg is T {
  if (!check(arg)) {
    throw new Error(`Violators found: ${JSON.stringify(arg)}`)
  }
}

export interface ISuccessResponse {
  data: Record<string, any>
}

function isSuccessResponse(arg: any): arg is ISuccessResponse {
  // TODO(leo.lin): check the error object to make sure there is no errors
  return typeof arg.data === 'object'
}

export function assertIsSuccessResponse(arg: any) {
  assertIsTyped(arg, isSuccessResponse)
  return arg
}
