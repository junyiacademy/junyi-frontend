/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Popover from '@material-ui/core/Popover'
import Divider from '@material-ui/core/Divider'
import Button from '@material-ui/core/Button'

// custom types
import type { ISubject } from '../interfaces/subject'

// utils

// assets

// actions

// components
import { BaseNextLink } from '@/packages/base'
import ItemList from './ItemList'

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    popoverRoot: {
      display: 'flex',
      justifyContent: 'center',
    },
    popoverPaper: {
      maxWidth: '100%',
      position: 'absolute',
      top: theme.spacing(8),
      [theme.breakpoints.down('xs')]: {
        top: theme.spacing(7),
      },
      width: '100%',
      [theme.breakpoints.up('lg')]: {
        width: '80%',
      },
    },
    divider: {
      backgroundColor: '#74C660',
      width: '100%',
      height: 2,
    },
    subject: {
      margin: theme.spacing(2.5, 1),
    },
    subjectButtonRoot: {
      display: 'block',
      padding: theme.spacing(0.5, 1),
    },
    subjectButtonLabel: {
      color: '#2A79D6',
      lineHeight: '26px',
      fontSize: '21px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    groups: {
      marginTop: theme.spacing(0.5),
    },
  })
)

const CourseMenu = ({
  subjects,
  courseMenuAnchorEl,
  onCourseMenuClose,
}: Props) => {
  const classes = useStyles()

  const isCourseMenuOpen = Boolean(courseMenuAnchorEl)

  const courseMenuId = 'course-menu'

  return (
    <Popover
      id={courseMenuId}
      open={isCourseMenuOpen}
      anchorEl={courseMenuAnchorEl}
      onClose={onCourseMenuClose}
      anchorReference={'none'}
      classes={{
        root: classes.popoverRoot,
        paper: classes.popoverPaper,
      }}
    >
      <Grid container direction='row'>
        {subjects.map(({ title, href, groups }) => {
          return (
            <div className={classes.subject} key={`subject-${title}`}>
              <BaseNextLink href={href}>
                <Button
                  component='a'
                  disabled={href === ''}
                  disableRipple
                  classes={{
                    root: classes.subjectButtonRoot,
                    label: classes.subjectButtonLabel,
                  }}
                >
                  {title}
                </Button>
              </BaseNextLink>
              <Grid
                container
                direction='row'
                justify='flex-start'
                wrap='wrap'
                classes={{ root: classes.groups }}
              >
                <Divider classes={{ root: classes.divider }} />
                {groups.map(({ title, items }) => (
                  <ItemList key={`item-${title}`} title={title} items={items} />
                ))}
              </Grid>
            </div>
          )
        })}
      </Grid>
    </Popover>
  )
}

type Props = {
  subjects: ISubject[]
  courseMenuAnchorEl: null | HTMLElement
  onCourseMenuClose: () => void
}

export default CourseMenu
