/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

// custom types
import type { ISubjectGroup } from '../interfaces/subject'

// utils

// assets

// actions

// components

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listItemRoot: {
      width: '145px',
      margin: 0,
      padding: theme.spacing(0.5),
      [theme.breakpoints.down('xs')]: {
        margin: theme.spacing(0, 0.5),
      },
    },
    listItemTextRoot: {
      margin: 0,
    },
    listItemTextPrimaryGroup: {
      padding: theme.spacing(0, 0.5),
      fontSize: '18px',
      color: '#B8B8B8',
    },
    listItemTextPrimary: {
      padding: theme.spacing(0.5),
      fontSize: '16px',
      color: '#444444',
      '&:hover': {
        color: '#F09716',
        backgroundColor: '#F3F8FF',
        cursor: 'pointer',
      },
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
  })
)

const ItemList = ({ title, items }: Props) => {
  const classes = useStyles()

  return (
    <List component='nav' aria-label='main course'>
      {title && (
        <ListItem classes={{ root: classes.listItemRoot }}>
          <ListItemText
            classes={{
              root: classes.listItemTextRoot,
              primary: classes.listItemTextPrimaryGroup,
            }}
            primary={title}
          />
        </ListItem>
      )}
      {items.map(({ title, href }) => (
        <ListItem
          key={`item-${title}`}
          classes={{ root: classes.listItemRoot }}
          component='a'
          href={href}
        >
          <ListItemText
            classes={{
              root: classes.listItemTextRoot,
              primary: classes.listItemTextPrimary,
            }}
            primary={title}
          />
        </ListItem>
      ))}
    </List>
  )
}

type Props = ISubjectGroup

export default ItemList
