/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import type { IRootState } from '@/store'

// utils

// assets

// actions
import { fetchCourseMenuAsync } from '../redux'

// components
import CourseMenuComponent from '../components/CourseMenu'

// self-defined-components

const CourseMenu = ({
  subjects,
  fetchCourseMenuAsync,
  courseMenuAnchorEl,
  onCourseMenuClose,
}: Props) => {
  React.useEffect(() => {
    if (subjects.length === 0) {
      fetchCourseMenuAsync()
    }
  }, [subjects, fetchCourseMenuAsync])

  return (
    <CourseMenuComponent
      subjects={subjects}
      courseMenuAnchorEl={courseMenuAnchorEl}
      onCourseMenuClose={onCourseMenuClose}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  subjects: state.courseMenu.subjects,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchCourseMenuAsync }, dispatch)

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {
  courseMenuAnchorEl: null | HTMLElement
  onCourseMenuClose: () => void
}
type Props = PropsFromRedux & PropsFromSelf

export default connector(CourseMenu)
