/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { createSlice, createAction, PayloadAction } from '@reduxjs/toolkit'
import { ISubject } from '../interfaces/subject'
import type { IState } from './types'

export const namespace = 'courseMenu'

/**
 * Ex: math / english / ...
 * @typedef {Object} Subject
 * @property {string} title - subject title
 * @property {string} href - link for this subject
 * @property {Group[]} groups
 *
 * Ex: elementary school / junior high school / ...
 * @typedef {Object} Group
 * @property {string} title - group title
 * @property {Item[]} items
 *
 * Ex: grade 1 / grade 2 / ...
 * @typedef {Object} Item
 * @property {string} title - item title
 * @property {string} href - link for this item
 */

const initialState: IState = {
  /** @type {Subject[]} */ subjects: [],
}

const courseMenuSlice = createSlice({
  name: namespace,
  initialState,
  reducers: {
    fetchCourseMenuSuccess: (
      state,
      action: PayloadAction<{ subjects: ISubject[] }>
    ) => {
      state.subjects = action.payload.subjects
    },
    fetchCourseMenuFailure: (state) => {
      state.subjects = initialState.subjects
    },
  },
})

export const fetchCourseMenuAsync = createAction(
  `${namespace}/fetchCourseMenuAsync`
)

export const {
  fetchCourseMenuSuccess,
  fetchCourseMenuFailure,
} = courseMenuSlice.actions

export const reducer = courseMenuSlice.reducer
