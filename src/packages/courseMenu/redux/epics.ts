/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics, Epic, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { assertIsSuccessResponse } from '@/utils'
import { assertIsCourseMenuResponse } from './types'

import {
  fetchCourseMenuAsync,
  fetchCourseMenuSuccess,
  fetchCourseMenuFailure,
} from './slice'

export const courseMenuEpic: Epic = (action$, _state$, { get }) =>
  action$.pipe(
    ofType(fetchCourseMenuAsync.type),
    mergeMap(() =>
      from(get('/api/course-menu', {})).pipe(
        map(assertIsSuccessResponse),
        map(assertIsCourseMenuResponse),
        map((response) => response.data),
        map(fetchCourseMenuSuccess),
        catchError(() => of(fetchCourseMenuFailure()))
      )
    )
  )

export default combineEpics(courseMenuEpic)
