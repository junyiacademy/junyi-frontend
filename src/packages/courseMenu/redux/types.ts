import { assertIsTyped } from '@/utils'
import type { ISuccessResponse } from '@/utils'
import type { ISubject } from '../interfaces/subject'

export interface IState {
  subjects: ISubject[]
}

interface ICourseMenuResponse {
  data: {
    subjects: ISubject[]
  }
}

function isCourseMenuResponse(
  arg: ISuccessResponse
): arg is ICourseMenuResponse {
  return typeof arg.data.subjects === 'object'
}

export function assertIsCourseMenuResponse(arg: any) {
  assertIsTyped(arg, isCourseMenuResponse)
  return arg
}
