export interface ISubjectGroup {
  title: string
  items: {
    title: string
    href: string
  }[]
}

export interface ISubject {
  title: string
  href: string
  groups: ISubjectGroup[]
}
