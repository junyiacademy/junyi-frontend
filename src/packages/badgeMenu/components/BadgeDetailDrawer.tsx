/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { Box, SwipeableDrawer } from '@material-ui/core'

// custom types

// utils

// assets

// components
import BadgeDetail from '../containers/BadgeDetail'

// self-defined-configs

// self-defined-components

const BadgeDetailDrawer = ({ isDrawerOpen, setIsDrawerOpen }: Props) => {
  return (
    <SwipeableDrawer
      ModalProps={{
        // @ts-ignore
        'data-testid': 'badge-detail-drawer-modal',
      }}
      anchor='top'
      open={isDrawerOpen}
      onOpen={() => {
        setIsDrawerOpen({ isDrawerOpen: true })
      }}
      onClose={() => {
        setIsDrawerOpen({ isDrawerOpen: false })
      }}
      transitionDuration={{
        enter: 500,
        exit: 500,
      }}
    >
      <Box data-testid='badge-detail-drawer-content' height='450px'>
        <BadgeDetail />
      </Box>
    </SwipeableDrawer>
  )
}

type Props = {
  isDrawerOpen: boolean
  setIsDrawerOpen: (arg: { isDrawerOpen: boolean }) => void
}

export default BadgeDetailDrawer
