/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Box } from '@material-ui/core'

// custom types
import type IBadgeCollection from '../interfaces/badgeCollection'

// utils

// assets

// components
import { Container, Item } from '@/packages/base'
import CategoryDetail from '../containers/CategoryDetail'
import CategoryTabs from './CategoryTabs'
import BadgeDetailDrawer from '../containers/BadgeDetailDrawer'

// self-defined-configs

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: theme.spacing(2),
      overflow: 'auto',
    },
  })
)

const BadgeMenu = ({
  badgeCollections,
  categoryIndex,
  setCategoryIndex,
}: Props) => {
  const classes = useStyles()

  return (
    <Box className={classes.root} data-testid='badge-menu-root'>
      <Container>
        <Item xs={12}>
          <CategoryTabs
            badgeCollections={badgeCollections}
            categoryIndex={categoryIndex}
            setCategoryIndex={setCategoryIndex}
          />
        </Item>
        <Item xs={12}>
          <CategoryDetail />
        </Item>
      </Container>
      <BadgeDetailDrawer />
    </Box>
  )
}

type Props = {
  badgeCollections: IBadgeCollection[]
  categoryIndex: number
  setCategoryIndex: (arg: { categoryIndex: number }) => void
}

export default BadgeMenu
