/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'

// utils
import { render, screen } from '@/tests'

// components
import CategoryTab from '../CategoryTab'

// self-defined-config
const TEST_CATEGORY_BUTTON_ID = 'category-button-root'
const TEST_CATEGORY_BUTTON_AVATAR_ID = 'category-button-avatar'

// self-defined-mocks
const mockIconUrl = 'fake icon url'
const mockTitle = 'fake title'
const mockOwnedBadgeCount = 3

test('should be rendered correctly with the provided props', () => {
  // Arrange
  const mockOnCategoryTabClick = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <CategoryTab
      title={mockTitle}
      icon={mockIconUrl}
      ownedBadgeCount={mockOwnedBadgeCount}
      isSelected={true}
      onCategoryTabClick={mockOnCategoryTabClick}
    />,
    options
  )

  // Assert
  const categoryTabEL = screen.getByTestId(TEST_CATEGORY_BUTTON_ID)
  expect(categoryTabEL).toBeInTheDocument()
  expect(categoryTabEL).toHaveStyle({
    position: 'relative',
    top: '25px',
    zIndex: '5',
  })
  expect(
    screen.getByTestId(TEST_CATEGORY_BUTTON_AVATAR_ID).firstChild
  ).toHaveAttribute('src', mockIconUrl)

  const categoryLabelEL = screen.getByText(
    `${mockTitle}徽章 × ${mockOwnedBadgeCount}`
  )
  expect(categoryLabelEL).toBeInTheDocument()
  expect(categoryLabelEL).toHaveStyle({
    border: '1px solid #bbb',
  })
})

test('should called onCategoryTabClick when clicked', () => {
  // Arrange
  const mockOnCategoryTabClick = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <CategoryTab
      title={mockTitle}
      icon={mockIconUrl}
      ownedBadgeCount={mockOwnedBadgeCount}
      isSelected={true}
      onCategoryTabClick={mockOnCategoryTabClick}
    />,
    options
  )

  // Act
  userEvent.click(screen.getByTestId(TEST_CATEGORY_BUTTON_ID))

  // Assert
  expect(mockOnCategoryTabClick).toBeCalledTimes(1)
})
