/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'

// utils
import { render, screen } from '@/tests'

// components
import Badge from '../Badge'

// self-defined-config
const TEST_BADGE_ID = 'badge-root'

// self-defined-mocks
const mockDescription = 'fake description'
const mockSafeExtendedDescription = 'fake safe extended description'
const mockIconUrl = 'fake icon url'
const mockTypeLabel = 'fake type label'

test('should be rendered correctly with the provided props', () => {
  // Arrange
  const mockSetIsDrawerOpen = jest.fn()
  const mockSetBadgeDetail = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <Badge
      isOwned={true}
      isUserBadge={true}
      iconUrl={mockIconUrl}
      description={mockDescription}
      lastEarnedTime={'1個月'}
      typeLabel={mockTypeLabel}
      safeExtendedDescription={mockSafeExtendedDescription}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      setBadgeDetail={mockSetBadgeDetail}
    />,
    options
  )

  // Act

  // Assert
  expect(screen.getByTestId(TEST_BADGE_ID)).toHaveStyle({
    opacity: '1',
    borderBottomColor: '#AAA',
    boxShadow: '0 1px 1px #CCC',
  })
  expect(screen.getByText(mockDescription)).toBeInTheDocument()
  expect(screen.getByText(mockSafeExtendedDescription)).toBeInTheDocument()
})

test('should call setBadgeDetail and setIsDrawerOpen with correct states when clicked', () => {
  // Arrange
  const mockSetIsDrawerOpen = jest.fn()
  const mockSetBadgeDetail = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <Badge
      isOwned={true}
      isUserBadge={true}
      iconUrl={mockIconUrl}
      description={mockDescription}
      lastEarnedTime={'1個月'}
      typeLabel={mockTypeLabel}
      safeExtendedDescription={mockSafeExtendedDescription}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      setBadgeDetail={mockSetBadgeDetail}
    />,
    options
  )

  // Act
  userEvent.click(screen.getByTestId(TEST_BADGE_ID))

  // Assert
  expect(mockSetIsDrawerOpen).toBeCalledTimes(1)
  expect(mockSetIsDrawerOpen).toBeCalledWith({ isDrawerOpen: true })
  expect(mockSetBadgeDetail).toBeCalledTimes(1)
  expect(mockSetBadgeDetail).toBeCalledWith({
    selectedBadge: {
      description: mockDescription,
      safeExtendedDescription: mockSafeExtendedDescription,
      iconUrl: mockIconUrl,
      typeLabel: mockTypeLabel,
    },
  })
})
