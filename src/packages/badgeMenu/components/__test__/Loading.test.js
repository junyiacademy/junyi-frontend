/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@/tests'

// components
import Loading from '../Loading'

// self-defined-config
const TEST_LOADING_COMPONENT_ID = 'loading-root'

test('should be rendered correctly', () => {
  // Arrange
  const options = {
    initialState: {},
  }

  render(<Loading />, options)

  // Act

  // Assert
  expect(screen.getByTestId(TEST_LOADING_COMPONENT_ID)).toBeInTheDocument()
})
