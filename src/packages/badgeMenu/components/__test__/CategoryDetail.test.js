/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@/tests'

// components
import CategoryDetail from '../CategoryDetail'

// self-defined-config
const TEST_BADGE_ID = 'badge-root'

// self-defined-mocks
const mockCategoryDescription = 'fake category description'
const mockLastEarnedDate = '2019-11-29T08:48:13Z'
const mockTypeLabel = 'fake type label'
const mockBadges = Array.from([1, 2, 3], (x) => ({
  description: `fake description ${x}`,
  icon: `fake icon url ${x}`,
  isOwned: true,
  lastEarnedDate: mockLastEarnedDate,
  safeExtendedDescription: `fake safe extended description ${x}`,
}))

test('should rendered user owned badges and all badges', () => {
  // Arrange
  const mockAllBadges = mockBadges
  const mockUserBadges = mockBadges

  const mockSetIsDrawerOpen = jest.fn()
  const mockSetBadgeDetail = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <CategoryDetail
      categoryDescription={mockCategoryDescription}
      userBadges={mockUserBadges}
      allBadges={mockAllBadges}
      typeLabel={mockTypeLabel}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      setBadgeDetail={mockSetBadgeDetail}
    />,
    options
  )

  // Act

  // Assert
  expect(screen.getByText(mockCategoryDescription)).toBeInTheDocument()
  expect(screen.getByText('已經得到的徽章')).toBeInTheDocument()
  expect(screen.getByText('未來可得的徽章')).toBeInTheDocument()
  expect(screen.getAllByTestId(TEST_BADGE_ID)).toHaveLength(
    mockUserBadges.length + mockAllBadges.length
  )

  mockUserBadges.forEach(({ description }) => {
    expect(screen.getAllByText(description)[0]).toBeInTheDocument()
  })

  const lastEarnedDate = `${parseInt(
    (new Date() - new Date(mockLastEarnedDate)) / (1000 * 60 * 60 * 24 * 30)
  )}個月`
  expect(screen.getAllByText(`${lastEarnedDate}前完成`)[0]).toBeInTheDocument()
})

test('should rendered all badges but not user owned badges', () => {
  // Arrange
  const mockAllBadges = mockBadges
  const mockUserBadges = []

  const mockSetIsDrawerOpen = jest.fn()
  const mockSetBadgeDetail = jest.fn()
  const options = {
    initialState: {},
  }

  render(
    <CategoryDetail
      categoryDescription={mockCategoryDescription}
      userBadges={mockUserBadges}
      allBadges={mockAllBadges}
      typeLabel={mockTypeLabel}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      setBadgeDetail={mockSetBadgeDetail}
    />,
    options
  )

  // Act

  // Assert
  expect(screen.getByText(mockCategoryDescription)).toBeInTheDocument()
  expect(screen.getByText('未來可得的徽章')).toBeInTheDocument()
  expect(screen.queryByText('已經得到的徽章')).not.toBeInTheDocument()
  expect(screen.getAllByTestId(TEST_BADGE_ID)).toHaveLength(
    mockUserBadges.length + mockAllBadges.length
  )

  mockAllBadges.forEach(({ description }) => {
    expect(screen.getByText(description)).toBeInTheDocument()
  })
})
