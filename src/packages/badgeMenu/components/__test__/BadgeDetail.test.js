/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@/tests'
import { createMatchMedia } from '@/tests/createMatchMedia'

// components
import BadgeDetail from '../BadgeDetail'

// self-defined-config
const SCREEN_WIDTH_DESKTOP = 1280
const SCREEN_WIDTH_MOBILE = 599
const TEST_AVATAR_ID = 'badge-detail-icon'
const TEST_SMALL_AVATAR_ID = 'badge-detail-small-icon'

// self-defined-mocks
const mockDescription = 'fake description'
const mockSafeExtendedDescription = 'fake safe extended description'
const mockIconUrl = 'fake icon url'
const mockTypeLabel = 'fake type label'

test('should be rendered correctly with the provided props (desktop)', () => {
  // Arrange
  window.matchMedia = createMatchMedia(SCREEN_WIDTH_DESKTOP)
  const options = {
    initialState: {},
  }

  render(
    <BadgeDetail
      description={mockDescription}
      safeExtendedDescription={mockSafeExtendedDescription}
      iconUrl={mockIconUrl}
      typeLabel={mockTypeLabel}
    />,
    options
  )

  // Act

  // Assert
  expect(screen.getByText(mockDescription)).toBeInTheDocument()
  expect(screen.getByText(mockSafeExtendedDescription)).toBeInTheDocument()
  expect(screen.getByText(`${mockTypeLabel}徽章`)).toBeInTheDocument()
  expect(screen.queryByTestId(TEST_SMALL_AVATAR_ID)).not.toBeInTheDocument()
  expect(screen.getByTestId(TEST_AVATAR_ID)).toBeInTheDocument()
  expect(screen.getByTestId(TEST_AVATAR_ID).firstChild).toHaveAttribute(
    'src',
    mockIconUrl
  )
})

test('should be rendered correctly with the provided props (mobile)', () => {
  // Arrange
  window.matchMedia = createMatchMedia(SCREEN_WIDTH_MOBILE)

  const options = {
    initialState: {},
  }

  render(
    <BadgeDetail
      description={mockDescription}
      safeExtendedDescription={mockSafeExtendedDescription}
      iconUrl={mockIconUrl}
      typeLabel={mockTypeLabel}
    />,
    options
  )

  // Act

  // Assert
  expect(screen.getByText(mockDescription)).toBeInTheDocument()
  expect(screen.getByText(mockSafeExtendedDescription)).toBeInTheDocument()
  expect(screen.getByText(`${mockTypeLabel}徽章`)).toBeInTheDocument()
  expect(screen.queryByTestId(TEST_AVATAR_ID)).not.toBeInTheDocument()
  expect(screen.getByTestId(TEST_SMALL_AVATAR_ID)).toBeInTheDocument()
  expect(screen.getByTestId(TEST_SMALL_AVATAR_ID).firstChild).toHaveAttribute(
    'src',
    mockIconUrl
  )
})
