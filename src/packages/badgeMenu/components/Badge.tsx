/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Avatar, Typography } from '@material-ui/core'

// custom types
import type ISelectedBadge from '../interfaces/selectedBadge'

// utils

// assets

// components

// self-defined-configs

// self-defined-components
const useStyles = makeStyles<Theme, { isOwned: boolean }>((theme: Theme) =>
  createStyles({
    badgeRoot: ({ isOwned }) => ({
      background: isOwned ? '#DDD' : '#EEE',
      border: `1px solid ${isOwned ? '#CCC' : '#DDD'}`,
      borderRadius: '5px',
      color: '#444444',
      position: 'relative',
      height: '128px',
      opacity: isOwned ? '1' : '0.75',
      borderBottomColor: isOwned ? '#AAA' : 'currentcolor',
      boxShadow: isOwned ? '0 1px 1px #CCC' : 'none',
      textShadow: isOwned ? '0 1px 0 #FFF' : 'none',
      '&:hover': {
        boxShadow: '0px 0px 4px 0px #aaa',
      },
    }),
    icon: {
      width: '90px',
      height: '90px',
      margin: theme.spacing(2, 0.8),
      float: 'left',
    },
    name: {
      fontWeight: 'bold',
      fontSize: 'large',
      lineHeight: 1.2,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
    subtext: {
      fontFamily: 'roboto',
      lineHeight: 1.4,
      whiteSpace: 'pre-wrap',
      overflow: 'hidden',
      marginBottom: theme.spacing(1),
    },
    textContainer: {
      padding: theme.spacing(1, 0.5),
    },
    subtextContainer: {
      fontSize: '14px',
      margin: theme.spacing(0.4),
      maxHeight: '84px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  })
)

const Badge = ({
  isOwned,
  isUserBadge,
  iconUrl,
  description,
  lastEarnedTime,
  typeLabel,
  safeExtendedDescription,
  setIsDrawerOpen,
  setBadgeDetail,
}: Props) => {
  const classes = useStyles({ isOwned })

  const handleBadgeClick = () => {
    setBadgeDetail({
      selectedBadge: {
        description,
        safeExtendedDescription,
        iconUrl,
        typeLabel,
      },
    })
    setIsDrawerOpen({ isDrawerOpen: true })
  }

  return (
    <div
      data-testid='badge-root'
      className={classes.badgeRoot}
      onClick={handleBadgeClick}
    >
      <Avatar className={classes.icon} alt='badge' src={iconUrl} />
      <div className={classes.textContainer}>
        <Typography className={classes.name}>{description}</Typography>
        <div className={classes.subtextContainer}>
          <Typography className={classes.subtext}>
            {safeExtendedDescription}
          </Typography>
          {isUserBadge && (
            <Typography className={classes.subtext}>
              {lastEarnedTime}前完成
            </Typography>
          )}
        </div>
      </div>
    </div>
  )
}

type Props = {
  isOwned: boolean
  isUserBadge: boolean
  iconUrl: string
  description: string
  lastEarnedTime: string
  typeLabel: string
  safeExtendedDescription: string
  setIsDrawerOpen: (arg: { isDrawerOpen: boolean }) => void
  setBadgeDetail: (arg: { selectedBadge: ISelectedBadge }) => void
}

export default Badge
