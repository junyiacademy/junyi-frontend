/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import {
  createStyles,
  makeStyles,
  Theme,
  useTheme,
} from '@material-ui/core/styles'
import { Avatar, Typography } from '@material-ui/core'

// custom types
import type ISelectedBadge from '../interfaces/selectedBadge'

// utils

// assets

// components
import { Container, Item } from '@/packages/base'

// self-defined-configs

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '380px',
      margin: theme.spacing(4.25, 3),
      padding: theme.spacing(3, 0),
      background: '#EEE',
      border: '1px solid #DDD',
      borderRadius: '5px',
      color: '#444444',
      position: 'relative',
    },
    icon: {
      height: 'auto',
      width: 'auto',
      maxHeight: '225px',
      maxWidth: '225px',
      margin: 'auto',
    },
    smallIcon: {
      height: '80px',
      width: '80px',
    },
    name: {
      fontWeight: 'bold',
      fontSize: '22px',
      whiteSpace: 'nowrap',
      marginBottom: theme.spacing(2.5),
      padding: theme.spacing(1, 2),
      border: '1px solid #aaa',
      backgroundColor: 'white',
      color: '#666',
    },
    subtext: {
      maxHeight: '192px',
      whiteSpace: 'pre-wrap',
      overflow: 'auto',
      marginBottom: theme.spacing(1),
    },
    textContainer: {
      fontSize: '14px',
      padding: theme.spacing(1, 3),
    },
    infoBox: {
      width: '280px',
      backgroundColor: '#fafafa',
      boxShadow: '0 1px 2px #ccc',
      position: 'absolute',
      padding: theme.spacing(1),
      right: '-7px',
      bottom: '30px',
    },
    infoText: {
      fontSize: '12px',
      lineHeight: '16px',
      textAlign: 'left',
      padding: theme.spacing(0, 1),
      margin: theme.spacing(1, 1),
      color: '#555',
    },
    colorText: {
      fontSize: '12px',
      lineHeight: '16px',
      display: 'inline',
      color: '#89ba40',
    },
    avatarText: {
      letterSpacing: '4px',
      textAlign: 'center',
      margin: theme.spacing(2.5, 0, 5),
      color: '#aaa',
    },
    iconContainer: {
      display: 'flex',
      justifyContent: 'center',
    },
  })
)

const BadgeDetail = ({
  description,
  safeExtendedDescription,
  iconUrl,
  typeLabel,
}: Props) => {
  const classes = useStyles()
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'))

  return (
    <div className={classes.root}>
      <Container>
        {isMobile ? (
          <Item xs={12} className={classes.iconContainer}>
            <Avatar
              data-testid='badge-detail-small-icon'
              className={classes.smallIcon}
              alt='badge'
              src={iconUrl}
            />
            <Typography className={classes.avatarText}>
              {typeLabel}徽章
            </Typography>
          </Item>
        ) : (
          <Item sm={4}>
            <Avatar
              data-testid='badge-detail-icon'
              className={classes.icon}
              alt='badge'
              src={iconUrl}
            />
            <Typography className={classes.avatarText}>
              {typeLabel}徽章
            </Typography>
          </Item>
        )}
        <Item sm={8} xs={12} className={classes.textContainer}>
          <Typography className={classes.name}>{description}</Typography>
          <Typography className={classes.subtext}>
            {safeExtendedDescription}
          </Typography>
        </Item>
      </Container>
      <div className={classes.infoBox}>
        <Typography className={classes.infoText}>
          這只是你在均一可以得到的上百個徽章之一
        </Typography>
        <Typography className={classes.infoText}>
          何不馬上
          <Typography className={classes.colorText} component={'span'}>
            開始練習
          </Typography>
          或
          <Typography className={classes.colorText} component={'span'}>
            看個影片
          </Typography>
          來得到徽章
        </Typography>
      </div>
    </div>
  )
}

type Props = ISelectedBadge

export default BadgeDetail
