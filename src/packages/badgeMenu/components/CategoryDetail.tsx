/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Box, Typography } from '@material-ui/core'

// custom types
import type IBadge from '../interfaces/badge'
import type ISelectedBadge from '../interfaces/selectedBadge'

// utils

// assets

// components
import { Container, Item } from '@/packages/base'
import Badge from './Badge'

// self-defined-configs

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingTop: theme.spacing(2),
      marginTop: theme.spacing(1.5),
      background: '#f7f7f7',
      border: '1px solid #bbb',
      borderRadius: '5px',
      boxShadow: 'inset 0 0 2px #ccc',
      transform: 'translateY(-30px)',
    },
    categoryDescription: {
      fontSize: '16px',
      textAlign: 'center',
      margin: theme.spacing(1, 0),
      padding: theme.spacing(0.5),
    },
    badgeBox: {
      padding: theme.spacing(1),
    },
    boxTitle: {
      display: 'block',
      marginLeft: theme.spacing(1),
    },
  })
)

/**
 * calculate months or days between the given date and today
 */
const dateTransfer = (date = Date()) => {
  const today = new Date()
  const earnedDate = new Date(date)
  const daysBetween = Math.round(
    (today.getTime() - earnedDate.getTime()) / (1000 * 60 * 60 * 24)
  )

  if (daysBetween > 30) {
    return `${Math.floor(daysBetween / 30)}個月`
  }

  return `${daysBetween}天`
}

const CategoryDetail = ({
  categoryDescription,
  userBadges,
  allBadges,
  typeLabel,
  setIsDrawerOpen,
  setBadgeDetail,
}: Props) => {
  const classes = useStyles()
  const categoryData = [
    {
      badges: userBadges,
      description: '已經得到的徽章',
      keyPrefix: 'user',
      isUserBadge: true,
    },
    {
      badges: allBadges,
      description: '未來可得的徽章',
      keyPrefix: 'all',
      isUserBadge: false,
    },
  ]

  return (
    <Box data-testid='category-box-root' className={classes.root}>
      <Typography className={classes.categoryDescription}>
        {categoryDescription}
      </Typography>
      {categoryData.map(
        ({ badges, description, keyPrefix, isUserBadge }) =>
          badges.length > 0 && (
            <Box key={`${keyPrefix}-badges`} className={classes.badgeBox}>
              <Typography className={classes.boxTitle}>
                {description}
              </Typography>
              <Container spacing={1}>
                {badges.map(
                  ({
                    description,
                    icon,
                    isOwned,
                    lastEarnedDate,
                    safeExtendedDescription,
                  }) => (
                    <Item
                      key={`${keyPrefix}-badge-${description}`}
                      xs={12}
                      sm={6}
                      lg={4}
                    >
                      <Badge
                        isOwned={isOwned}
                        isUserBadge={isUserBadge}
                        iconUrl={icon}
                        description={description}
                        lastEarnedTime={dateTransfer(lastEarnedDate)}
                        typeLabel={typeLabel}
                        safeExtendedDescription={safeExtendedDescription}
                        setIsDrawerOpen={setIsDrawerOpen}
                        setBadgeDetail={setBadgeDetail}
                      />
                    </Item>
                  )
                )}
              </Container>
            </Box>
          )
      )}
    </Box>
  )
}

type Props = {
  categoryDescription?: string
  userBadges: IBadge[]
  allBadges: IBadge[]
  typeLabel: string
  setIsDrawerOpen: (arg: { isDrawerOpen: boolean }) => void
  setBadgeDetail: (arg: { selectedBadge: ISelectedBadge }) => void
}

export default CategoryDetail
