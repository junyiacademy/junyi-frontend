export default interface ISelectedBadge {
  description: string;
  iconUrl: string;
  safeExtendedDescription: string;
  typeLabel: string;
}
  