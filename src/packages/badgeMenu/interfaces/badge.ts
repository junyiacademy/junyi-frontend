export default interface IBadge {
  description: string;
  icon: string;
  isOwned: boolean;
  lastEarnedDate: string;
  safeExtendedDescription: string;
}
