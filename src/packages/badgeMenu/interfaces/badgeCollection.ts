import type IBadge from './badge'

export default interface IBadgeCollection {
  category: number
  categoryIcon: string
  typeLabel: string
  badges: IBadge[]
  userBadges: IBadge[]
  categoryDescription: string
}
