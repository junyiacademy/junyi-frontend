/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics, Epic, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, switchMap, catchError } from 'rxjs/operators'
import { assertIsSuccessResponse } from '@/utils'
import { assertIsBadgeMenuResponse } from './types'

import {
  fetchBadgeMenuAsync,
  fetchBadgeMenuSuccess,
  fetchBadgeMenuFailure,
} from './slice'

export const fetchBadgeMenuEpic: Epic = (action$, _state$, { get }) =>
  action$.pipe(
    ofType(fetchBadgeMenuAsync.type),
    switchMap(() =>
      from(get('/api/badge-menu', {})).pipe(
        map(assertIsSuccessResponse),
        map(assertIsBadgeMenuResponse),
        map((response) => response.data),
        map(fetchBadgeMenuSuccess),
        catchError(() => of(fetchBadgeMenuFailure()))
      )
    )
  )

export default combineEpics(fetchBadgeMenuEpic)
