/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { TestScheduler } from 'rxjs/testing'
import {
  fetchBadgeMenuAsync,
  fetchBadgeMenuSuccess,
  fetchBadgeMenuFailure,
} from '../slice'
import { fetchBadgeMenuEpic } from '../epics'

test('badgeMenuEpic success case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  const BADGE_MENU_RESPONSE = {
    data: { badgeCollections: [] },
  }

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: fetchBadgeMenuAsync(),
    })
    const state$ = null
    const dependencies = {
      get: () =>
        cold('--a', {
          a: BADGE_MENU_RESPONSE,
        }),
    }

    const output$ = fetchBadgeMenuEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: fetchBadgeMenuSuccess.type,
        payload: BADGE_MENU_RESPONSE.data,
      },
    })
  })
})

test('badgeMenuEpic error case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: fetchBadgeMenuAsync(),
    })
    const state$ = null
    const dependencies = {
      get: () => cold('--#'),
    }

    const output$ = fetchBadgeMenuEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: fetchBadgeMenuFailure.type,
        payload: undefined,
      },
    })
  })
})
