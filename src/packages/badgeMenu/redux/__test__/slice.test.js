import {
  reducer,
  initialState,
  fetchBadgeMenuAsync,
  fetchBadgeMenuSuccess,
  fetchBadgeMenuFailure,
  setIsDrawerOpen,
  setBadgeDetail,
  setCategoryIndex,
} from '../slice'

test('should return the initial state on first run', () => {
  // Arrange
  const nextState = initialState

  // Act
  const result = reducer(undefined, {})

  // Assert
  expect(result).toEqual(nextState)
})

test('should properly set the state when a fetch badge menu request is made', () => {
  // Arrange

  // Act
  const nextState = reducer(initialState, fetchBadgeMenuAsync())

  // Assert
  expect(nextState.categoryIndex).toEqual(0)
  expect(nextState.isLoading).toEqual(true)
})

test('should properly set the state when a fetch badge menu request succeeds', () => {
  // Arrange
  const mockInitialState = {
    ...initialState,
    isLoading: true,
  }
  const payload = {
    badgeCollections: [
      {
        description: 'fake description',
        icon: 'fake icon url',
        isOwned: true,
        lastEarnedDate: 'fake last earned date',
        safeExtendedDescription: 'fake safe extended description',
      },
    ],
  }

  // Act
  const nextState = reducer(mockInitialState, fetchBadgeMenuSuccess(payload))

  // Assert
  expect(nextState.badgeCollections).toEqual(payload.badgeCollections)
  expect(nextState.isLoading).toEqual(false)
})

test('should properly set the state when a fetch badge menu request fails', () => {
  // Arrange
  const mockInitialState = {
    ...initialState,
    isLoading: true,
    badgeCollections: [
      {
        description: 'fake description',
        icon: 'fake icon url',
        isOwned: true,
        lastEarnedDate: 'fake last earned date',
        safeExtendedDescription: 'fake safe extended description',
      },
    ],
  }

  // Act
  const nextState = reducer(mockInitialState, fetchBadgeMenuFailure())

  // Assert
  expect(nextState.isLoading).toEqual(false)
  expect(nextState.badgeCollections).toEqual(initialState.badgeCollections)
})

test('should properly set the state when a set is drawer open action is dispatched', () => {
  // Arrange
  const payload = {
    isDrawerOpen: true,
  }

  // Act
  const nextState = reducer(initialState, setIsDrawerOpen(payload))

  // Assert
  expect(nextState.isDrawerOpen).toEqual(payload.isDrawerOpen)
})

test('should properly set the state when a set badge detail action is dispatched', () => {
  // Arrange
  const payload = {
    selectedBadge: {
      description: 'fake description',
      safeExtendedDescription: 'fake safe extended description',
      iconUrl: 'fake icon url',
    },
  }

  // Act
  const nextState = reducer(initialState, setBadgeDetail(payload))

  // Assert
  expect(nextState.selectedBadge).toEqual(payload.selectedBadge)
})

test('should properly set the state when a set category index action is dispatched', () => {
  // Arrange
  const payload = {
    categoryIndex: 2,
  }

  // Act
  const nextState = reducer(initialState, setCategoryIndex(payload))

  // Assert
  expect(nextState.categoryIndex).toEqual(payload.categoryIndex)
})
