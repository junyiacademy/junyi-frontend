import { selectCurrentBadgeCollection } from '../selectors'
import { namespace } from '../slice'

// self-defined-mock
const initData = {
  badges: [],
  category: 0,
  categoryIcon: '',
  categoryDescription: '',
  typeLabel: '隕石',
  userBadges: [],
}
const mockBadgeCollections = Array.from([0, 1, 2, 3], (x) => ({
  badges: [],
  category: x,
  categoryIcon: '',
  typeLabel: `type-label-${x}`,
  userBadges: [],
}))

test('selectCurrentBadgeCollection should return badgeCollection of that category', () => {
  expect(
    selectCurrentBadgeCollection({
      [namespace]: {
        categoryIndex: 1,
        badgeCollections: mockBadgeCollections,
      },
    })
  ).toStrictEqual(mockBadgeCollections[1])
})

test('selectCurrentBadgeCollection should return init data when category index < 0', () => {
  expect(
    selectCurrentBadgeCollection({
      [namespace]: {
        categoryIndex: -1,
        badgeCollections: mockBadgeCollections,
      },
    })
  ).toStrictEqual(initData)
})

test('selectCurrentBadgeCollection should return init data when category index > badge collection length', () => {
  expect(
    selectCurrentBadgeCollection({
      [namespace]: {
        categoryIndex: -1,
        badgeCollections: mockBadgeCollections,
      },
    })
  ).toStrictEqual(initData)
})
