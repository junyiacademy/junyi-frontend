/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type IBadgeCollection from '../interfaces/badgeCollection'
import type ISelectedBadge from '../interfaces/selectedBadge'
import type { IState } from './types'

export const namespace = 'badgeMenu'

/**
 * Ex: meteorite / moon / ...
 * @typedef {Object} BadgeCollection
 * @property {number} category - the key num of the category
 * @property {string} categoryIcon - the icon of the category
 * @property {string} typeLabel - label of the category
 * @property {Badge[]} badges - all badges of the category
 * @property {Badge[]} userBadges - all badges that user owned of the category
 *
 * data of a single badge
 * @typedef {Object} Badge
 * @property {string} description - the name of the badge
 * @property {string} icon - icon of the badge
 * @property {boolean} isOwned - if user have it
 * @property {string} lastEarnedDate - last earned date of the badge
 * @property {string} safeExtendedDescription - long description of the badge
 *
 * badge detail in drawer
 * @typedef {Object} SelectedBadge
 * @property {string} description - the name of the badge
 * @property {string} safeExtendedDescription - long description of the badge
 * @property {string} iconUrl - icon url of the badge
 */

export const initialState: IState = {
  // server side
  /** @type {BadgeCollection[]} */ badgeCollections: [],

  // client side
  /** @type {boolean} */ isLoading: false,
  /** @type {number} */ categoryIndex: -1,
  /** @type {SelectedBadge} */ selectedBadge: {
    description: '錯誤',
    safeExtendedDescription: '',
    iconUrl: '',
    typeLabel: '',
  },
  /** @type {boolean} */ isDrawerOpen: false,
}

const badgeMenuSlice = createSlice({
  name: namespace,
  initialState,
  reducers: {
    fetchBadgeMenuAsync: (state) => {
      state.categoryIndex = 0
      state.isLoading = true
    },
    fetchBadgeMenuSuccess: (
      state,
      action: PayloadAction<{ badgeCollections: IBadgeCollection[] }>
    ) => {
      state.badgeCollections = action.payload.badgeCollections
      state.isLoading = false
    },
    fetchBadgeMenuFailure: (state) => {
      state.badgeCollections = initialState.badgeCollections
      state.isLoading = false
    },
    setIsDrawerOpen: (
      state,
      action: PayloadAction<{ isDrawerOpen: boolean }>
    ) => {
      state.isDrawerOpen = action.payload.isDrawerOpen
    },
    setBadgeDetail: (
      state,
      action: PayloadAction<{
        selectedBadge: ISelectedBadge
      }>
    ) => {
      state.selectedBadge = action.payload.selectedBadge
    },
    setCategoryIndex: (
      state,
      action: PayloadAction<{ categoryIndex: number }>
    ) => {
      state.categoryIndex = action.payload.categoryIndex
    },
  },
})

export const {
  fetchBadgeMenuAsync,
  fetchBadgeMenuSuccess,
  fetchBadgeMenuFailure,
  setIsDrawerOpen,
  setBadgeDetail,
  setCategoryIndex,
} = badgeMenuSlice.actions

export const reducer = badgeMenuSlice.reducer
