import { createSelector } from '@reduxjs/toolkit'
import type { IRootState } from '@/store'

const selectCategoryIndex = (state: IRootState) => state.badgeMenu.categoryIndex
const selectBadgeCollection = (state: IRootState) =>
  state.badgeMenu.badgeCollections

const selectCurrentBadgeCollection = createSelector(
  [selectCategoryIndex, selectBadgeCollection],
  (categoryIndex, badgeCollections) => {
    if (categoryIndex >= 0 && badgeCollections[categoryIndex]) {
      return badgeCollections[categoryIndex]
    }
    return {
      badges: [],
      category: 0,
      categoryIcon: '',
      categoryDescription: '',
      typeLabel: '隕石',
      userBadges: [],
    }
  }
)

export { selectCurrentBadgeCollection }
