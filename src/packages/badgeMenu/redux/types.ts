import { assertIsTyped } from '@/utils'
import type { ISuccessResponse } from '@/utils'
import type IBadgeCollection from '../interfaces/badgeCollection'
import type ISelectedBadge from '../interfaces/selectedBadge'

export interface IState {
  badgeCollections: IBadgeCollection[]
  isLoading: boolean
  selectedBadge: ISelectedBadge
  isDrawerOpen: boolean
  categoryIndex: number
}

interface IBadgeMenuResponse {
  data: {
    badgeCollections: IBadgeCollection[]
  }
}

function isBadgeMenuResponse(arg: ISuccessResponse): arg is IBadgeMenuResponse {
  return typeof arg.data.badgeCollections === 'object'
}

export function assertIsBadgeMenuResponse(arg: any) {
  assertIsTyped(arg, isBadgeMenuResponse)
  return arg
}
