/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'

// custom types
import { IRootState } from '@/store'

// utils

// assets

// selectors

// actions

// components
import BadgeDetailComponent from '../components/BadgeDetail'

// self-defined-configs

// self-defined-components
const BadgeDetail = ({
  description,
  safeExtendedDescription,
  iconUrl,
  typeLabel,
}: Props) => {
  return (
    <BadgeDetailComponent
      description={description}
      safeExtendedDescription={safeExtendedDescription}
      iconUrl={iconUrl}
      typeLabel={typeLabel}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  description: state.badgeMenu.selectedBadge.description,
  safeExtendedDescription:
    state.badgeMenu.selectedBadge.safeExtendedDescription,
  iconUrl: state.badgeMenu.selectedBadge.iconUrl,
  typeLabel: state.badgeMenu.selectedBadge.typeLabel,
})

const connector = connect(mapStateToProps, {})

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(BadgeDetail)
