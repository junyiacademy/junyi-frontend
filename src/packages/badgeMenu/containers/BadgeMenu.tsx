/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import { IRootState } from '@/store'

// utils

// assets

// selectors

// actions
import { fetchBadgeMenuAsync, setCategoryIndex } from '../redux'

// components
import BadgeMenuComponent from '../components/BadgeMenu'
import Loading from '../components/Loading'

// self-defined-configs

// self-defined-components
const BadgeMenu = ({
  badgeCollections,
  categoryIndex,
  isLoading,
  fetchBadgeMenuAsync,
  setCategoryIndex,
}: Props) => {
  React.useEffect(() => {
    fetchBadgeMenuAsync()
  }, [fetchBadgeMenuAsync])

  if (isLoading) {
    return <Loading />
  }

  return (
    <BadgeMenuComponent
      badgeCollections={badgeCollections}
      categoryIndex={categoryIndex}
      setCategoryIndex={setCategoryIndex}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  badgeCollections: state.badgeMenu.badgeCollections,
  categoryIndex: state.badgeMenu.categoryIndex,
  isLoading: state.badgeMenu.isLoading,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchBadgeMenuAsync,
      setCategoryIndex,
    },
    dispatch
  )

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(BadgeMenu)
