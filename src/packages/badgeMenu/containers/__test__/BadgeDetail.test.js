/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render } from '@/tests'

// components
import BadgeDetail from '../BadgeDetail'

test('should be rendered correctly', () => {
  // Arrange
  const options = {
    initialState: {},
  }

  render(<BadgeDetail />, options)

  // Act

  // Assert
})
