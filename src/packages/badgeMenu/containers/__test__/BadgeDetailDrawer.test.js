/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'

// utils
import { render, screen, waitFor } from '@/tests'
import { namespace, initialState } from '../../redux/slice'

// components
import BadgeDetailDrawer from '../BadgeDetailDrawer'

// self-defined-config
const TEST_ID_DRAWER_CONTENT = 'badge-detail-drawer-content'
const TEST_ID_MODAL = 'badge-detail-drawer-modal'

test('should be closed when modal was clicked', async () => {
  // Arrange
  const options = {
    initialState: {
      [namespace]: { ...initialState, isDrawerOpen: true },
    },
  }

  render(<BadgeDetailDrawer />, options)

  // Assert
  expect(screen.getByTestId(TEST_ID_DRAWER_CONTENT)).toBeInTheDocument()

  // Act
  userEvent.click(screen.getByTestId(TEST_ID_MODAL).firstChild)

  // Assert
  await waitFor(() =>
    expect(screen.queryByTestId(TEST_ID_DRAWER_CONTENT)).not.toBeInTheDocument()
  )
})
