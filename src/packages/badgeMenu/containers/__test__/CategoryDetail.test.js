/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@/tests'

// components
import CategoryDetail from '../CategoryDetail'

// self-defined-config
const TEST_CATEGORY_BOX_ID = 'category-box-root'

test('should be rendered correctly with the provided state', async () => {
  // Arrange
  const options = {
    initialState: {},
  }

  render(<CategoryDetail />, options)

  // Act

  // Assert
  expect(screen.getByTestId(TEST_CATEGORY_BOX_ID)).toBeInTheDocument()
})
