/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'
import { rest } from 'msw'
import { setupServer } from 'msw/node'

// utils
import { render, screen, waitFor } from '@/tests'

// components
import BadgeMenu from '../BadgeMenu'

// self-defined-config
const TEST_LOADING_COMPONENT_ID = 'loading-root'
const TEST_BADGE_MENU_ID = 'badge-menu-root'
const TEST_BADGE_ID = 'badge-root'
const TEST_DRAWER_ID = 'badge-detail-drawer-content'

// self-defined-mocks
const mockCategoryDescription = 'fake category description'
const mockBadgeCollections = [
  {
    badges: [
      {
        description: 'fake description',
        icon: 'localhost:3000/mock',
        isOwned: false,
        safeExtendedDescription: 'fake safe extended description',
      },
    ],
    category: 0,
    categoryDescription: 'fake category description',
    typeLabel: 'fake-type-label',
    categoryIcon: 'localhost:3000/mock',
    userBadges: [],
  },
]

// setup
const server = setupServer(
  rest.get('/api/badge-menu', (req, res, ctx) => {
    return res(
      ctx.json({
        badgeCollections: mockBadgeCollections,
      })
    )
  })
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('should be rendered correctly with the provided state', async () => {
  const options = {
    initialState: {},
  }

  render(<BadgeMenu />, options)

  // Act

  // Assert
  expect(screen.getByTestId(TEST_LOADING_COMPONENT_ID)).toBeInTheDocument()
  await waitFor(() =>
    expect(screen.getByTestId(TEST_BADGE_MENU_ID)).toBeInTheDocument()
  )
  expect(screen.getByText(mockCategoryDescription)).toBeInTheDocument()
  expect(screen.getByTestId(TEST_BADGE_ID)).toBeInTheDocument()
  expect(screen.getAllByTestId(TEST_BADGE_ID)).toHaveLength(1)
})

test('should pop out drawer when clicked badge', async () => {
  const options = {
    initialState: {},
  }

  render(<BadgeMenu />, options)

  // Asset
  await waitFor(() =>
    expect(screen.getByTestId(TEST_BADGE_ID)).toBeInTheDocument()
  )

  // Act
  userEvent.click(screen.getByTestId(TEST_BADGE_ID))

  // Assert
  expect(screen.getByTestId(TEST_DRAWER_ID)).toBeInTheDocument()
})

test('fetch menu error case', async () => {
  // Arrange
  server.use(
    rest.get('/api/badge-menu', (req, res, ctx) => {
      return res(ctx.status(500))
    })
  )

  const options = {
    initialState: {},
  }

  render(<BadgeMenu />, options)

  // Act

  // Assert
})
