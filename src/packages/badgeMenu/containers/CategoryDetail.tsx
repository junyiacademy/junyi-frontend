/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import { IRootState } from '@/store'

// utils

// assets

// selectors
import { selectCurrentBadgeCollection } from '../redux/selectors'

// actions
import { setIsDrawerOpen, setBadgeDetail } from '../redux'

// components
import CategoryDetailComponent from '../components/CategoryDetail'

// self-defined-configs

// self-defined-components
const CategoryDetail = ({
  categoryDescription,
  userBadges,
  allBadges,
  typeLabel,
  setIsDrawerOpen,
  setBadgeDetail,
}: Props) => {
  return (
    <CategoryDetailComponent
      categoryDescription={categoryDescription}
      userBadges={userBadges}
      allBadges={allBadges}
      typeLabel={typeLabel}
      setIsDrawerOpen={setIsDrawerOpen}
      setBadgeDetail={setBadgeDetail}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  categoryDescription: selectCurrentBadgeCollection(state).categoryDescription,
  userBadges: selectCurrentBadgeCollection(state).userBadges,
  allBadges: selectCurrentBadgeCollection(state).badges,
  typeLabel: selectCurrentBadgeCollection(state).typeLabel,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      setIsDrawerOpen,
      setBadgeDetail,
    },
    dispatch
  )

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(CategoryDetail)
