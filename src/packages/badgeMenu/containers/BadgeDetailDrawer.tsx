/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import { IRootState } from '@/store'

// utils

// assets

// selectors

// actions
import { setIsDrawerOpen } from '../redux'

// components
import BadgeDetailDrawerComponent from '../components/BadgeDetailDrawer'

// self-defined-configs

// self-defined-components
const BadgeDetailDrawer = ({ isDrawerOpen, setIsDrawerOpen }: Props) => {
  return (
    <BadgeDetailDrawerComponent
      isDrawerOpen={isDrawerOpen}
      setIsDrawerOpen={setIsDrawerOpen}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  isDrawerOpen: state.badgeMenu.isDrawerOpen,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ setIsDrawerOpen }, dispatch)

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(BadgeDetailDrawer)
