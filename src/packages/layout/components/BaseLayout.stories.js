/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

import BaseLayout from './BaseLayout'

export default {
  title: 'Layout/BaseLayout',
  component: BaseLayout,
}

const Template = (args) => <BaseLayout {...args} />

export const Primary = Template.bind({})
Primary.args = {}
