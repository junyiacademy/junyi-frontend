/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@/tests'

// components
import StudentLayout from '../StudentLayout'

describe('StudentLayout', () => {
  it('renders without crashing', () => {
    // Arrange
    const options = {
      initialState: {},
    }

    render(<StudentLayout />, options)

    // Act

    // Assert
    expect(screen.getByRole('img', { name: 'logo' })).toBeInTheDocument()
  })
})
