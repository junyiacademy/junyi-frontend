/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

import Header from './Header'

export default {
  title: 'Layout/Header',
  component: Header,
}

const Template = (args) => <Header {...args} />

export const LoggedOut = Template.bind({})
LoggedOut.args = {}
