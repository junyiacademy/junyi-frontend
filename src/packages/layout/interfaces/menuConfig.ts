export interface IMenuConfig {
  route: string
  title: string
  icon: React.ReactNode | Function
}
