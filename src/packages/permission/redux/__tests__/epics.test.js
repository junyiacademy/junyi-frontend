/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { TestScheduler } from 'rxjs/testing'
import {
  loginSuccess,
  loginFailure,
  loginAsync,
  fetchPermissionSuccess,
  fetchPermissionFailure,
  fetchPermissionAsync,
} from '../slice'
import { loginEpic, permissionEpic } from '../epics'

test('loginEpic happy case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  const LOGIN_RESPONSE = {
    foo: 'bar',
  }

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: loginAsync(),
    })
    const state$ = null
    const dependencies = {
      post: () =>
        cold('--a', {
          a: LOGIN_RESPONSE,
        }),
    }

    const output$ = loginEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: loginSuccess.type,
        payload: undefined,
      },
    })
  })
})

test('loginEpic sad case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: loginAsync(),
    })
    const state$ = null
    const dependencies = {
      post: () => cold('--#'),
    }

    const output$ = loginEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: loginFailure.type,
        payload: undefined,
      },
    })
  })
})

test('permissionEpic happy case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  const FETCH_PERMISSION_RESPONSE = {
    data: { roles: [] },
  }

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: fetchPermissionAsync(),
    })
    const state$ = null
    const dependencies = {
      get: () =>
        cold('--a', {
          a: FETCH_PERMISSION_RESPONSE,
        }),
    }

    const output$ = permissionEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: fetchPermissionSuccess.type,
        payload: FETCH_PERMISSION_RESPONSE.data,
      },
    })
  })
})

test('permissionEpic sad case', () => {
  const testScheduler = new TestScheduler((actual, expected) => {
    expect(actual).toStrictEqual(expected)
  })

  testScheduler.run(({ hot, cold, expectObservable }) => {
    const action$ = hot('-a-a', {
      a: fetchPermissionAsync(),
    })
    const state$ = null
    const dependencies = {
      get: () => cold('--#'),
    }

    const output$ = permissionEpic(action$, state$, dependencies)

    expectObservable(output$).toBe('-----a', {
      a: {
        type: fetchPermissionFailure.type,
        payload: undefined,
      },
    })
  })
})
