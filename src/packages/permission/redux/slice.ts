/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { IState } from './types'

export const namespace = 'permission'

const initialState: IState = {
  isLoginProcessing: false,
  roles: [], // ['admin', 'developer', 'moderator', 'user']
}

const permissionSlice = createSlice({
  name: namespace,
  initialState,
  reducers: {
    loginAsync: (state) => {
      state.isLoginProcessing = true
    },
    loginSuccess: (state) => {
      state.isLoginProcessing = false
    },
    loginFailure: (state) => {
      state.isLoginProcessing = false
    },
    fetchPermissionSuccess: (
      state,
      action: PayloadAction<{ roles: string[] }>
    ) => {
      state.roles = action.payload.roles
    },
    fetchPermissionFailure: (state) => {
      state.roles = initialState.roles
    },
  },
})

export const fetchPermissionAsync = createAction(
  `${namespace}/fetchPermissionAsync`
)

export const {
  loginAsync,
  loginSuccess,
  loginFailure,
  fetchPermissionSuccess,
  fetchPermissionFailure,
} = permissionSlice.actions

export const reducer = permissionSlice.reducer
