/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineEpics, Epic, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, switchMap, catchError } from 'rxjs/operators'
import { assertIsSuccessResponse } from '@/utils'
import { assertIsPermissionResponse } from './types'

import {
  loginSuccess,
  loginFailure,
  loginAsync,
  fetchPermissionSuccess,
  fetchPermissionFailure,
  fetchPermissionAsync,
} from './slice'

export const loginEpic: Epic = (action$, _state$, { post }) =>
  action$.pipe(
    ofType(loginAsync.type),
    switchMap(() =>
      from(
        post('/api/login', { email: 'wtlin1228', password: 'changeme' })
      ).pipe(
        map(() => loginSuccess()),
        catchError(() => of(loginFailure()))
      )
    )
  )

export const permissionEpic: Epic = (action$, _state$, { get }) =>
  action$.pipe(
    ofType(fetchPermissionAsync.type),
    switchMap(() =>
      from(get('/api/permission', {})).pipe(
        map(assertIsSuccessResponse),
        map(assertIsPermissionResponse),
        map((response) => response.data),
        map(fetchPermissionSuccess),
        catchError(() => of(fetchPermissionFailure()))
      )
    )
  )

export default combineEpics(loginEpic, permissionEpic)
