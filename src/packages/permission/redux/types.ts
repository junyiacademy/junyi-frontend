import { assertIsTyped } from '@/utils'
import type { ISuccessResponse } from '@/utils'

export interface IState {
  isLoginProcessing: boolean
  roles: string[]
}

interface IPermissionResponse {
  data: {
    roles: string[]
  }
}

function isPermissionResponse(
  arg: ISuccessResponse
): arg is IPermissionResponse {
  return typeof arg.data.roles === 'object'
}

export function assertIsPermissionResponse(arg: any) {
  assertIsTyped(arg, isPermissionResponse)
  return arg
}
