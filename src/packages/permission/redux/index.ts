/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export { namespace, reducer, loginAsync, fetchPermissionAsync } from './slice'

export { default as epic } from './epics'
