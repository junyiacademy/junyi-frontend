/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils

// assets

// actions

// components

// self-defined-components

const Login = ({ isLoginProcessing, onLogin }: Props) => {
  const handleLoginButtonClick: React.FormEventHandler = (e) => {
    e.preventDefault()
    onLogin()
  }
  return (
    <div>
      <form onSubmit={handleLoginButtonClick}>
        <button type='submit' disabled={isLoginProcessing}>
          {isLoginProcessing ? '登入中' : '點我登入'}
        </button>
      </form>
    </div>
  )
}

type Props = {
  isLoginProcessing: boolean
  onLogin: () => void
}

export default Login
