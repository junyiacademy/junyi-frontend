/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { useRouter } from 'next/router'
import { connect, ConnectedProps } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

// custom types
import type { IRootState } from '@/store'

// utils
import { getAuthToken } from '../utils'

// assets

// actions
import { loginAsync } from '../redux'

// components
import LoginComponent from '../components/Login'

// self-defined-components

const Login = ({ isLoginProcessing, loginAsync }: Props) => {
  const router = useRouter()

  if (getAuthToken()) {
    router.push('/')
  }

  return (
    <LoginComponent
      isLoginProcessing={isLoginProcessing}
      onLogin={loginAsync}
    />
  )
}

const mapStateToProps = (state: IRootState) => ({
  isLoginProcessing: state.permission.isLoginProcessing,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ loginAsync }, dispatch)

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>
type PropsFromSelf = {}
type Props = PropsFromRedux & PropsFromSelf

export default connector(Login)
