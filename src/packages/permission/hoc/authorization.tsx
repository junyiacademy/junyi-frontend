/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'

// custom types
import type { IRootState } from '@/store'

// utils
import { getAuthToken } from '../utils'

// assets

// actions
import { fetchPermissionAsync } from '../redux'

// components
import { BaseLayout } from '@/packages/layout'

// self-defined-components

const Authorization = (allowedRole: string) => (
  WrappedComponent: React.ComponentType
) => {
  const usePermission = () => {
    const dispatch = useDispatch()

    return {
      permission: useSelector((state: IRootState) => state.permission),
      fetchPermission: () => dispatch(fetchPermissionAsync()),
    }
  }

  const WithAuthorization = ({ serverSideAuthToken = '' }) => {
    const router = useRouter()
    const isServerSide = typeof window === 'undefined'

    const { permission, fetchPermission } = usePermission()
    const authToken = serverSideAuthToken ?? getAuthToken()
    const isAuthenticated = permission.roles.includes(allowedRole)

    useEffect(() => {
      if (!isServerSide && authToken && !isAuthenticated) {
        fetchPermission()
      }
    }, [isServerSide, authToken, isAuthenticated])

    if (!isServerSide && !authToken) {
      router.push('/login')
    }

    if (isAuthenticated) {
      return <WrappedComponent />
    }

    return <BaseLayout>You don't have permission to view this page</BaseLayout>
  }

  return WithAuthorization
}

export default Authorization
